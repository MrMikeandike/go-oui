# Purpose

## 1: Parse ieee oui
1. get https://linuxnet.ca/ieee/oui.txt.zip or another variant from https://linuxnet.ca/ieee/oui
2. parse output into obj with OUI, Vendor, etc
3. write data to be read again later
    * maybe from csv, maybe from json, maybe from boltdb, maybe from sql, maybe from sqlite

## 2: Go Functions to filter data
1. Get by OUI
2. Maybe get by vendor

## 3: Api that hosts it all
1. GET methods
    * /api/oui
2. Auto update from previous steps
