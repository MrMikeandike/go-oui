module gitlab.com/MrMikeandike/go-oui

go 1.13

require (
	github.com/asdine/storm v2.1.2+incompatible
	github.com/stretchr/testify v1.5.1 // indirect
	go.etcd.io/bbolt v1.3.3
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
)
