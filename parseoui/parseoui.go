package parseoui

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/MrMikeandike/go-oui/ouiconf"
)

// savedFilename is the filename the ouilist txt file will be saved as
var savedFilename = "oui.txt"

func ParseText(conf ouiconf.Config) error {
	// Verify used Config values
	err := conf.AppDir.Verify()
	if err != nil {
		return err
	}

	// Open file, create scanner that splits on newlines
	f, err := os.Open(fmt.Sprintf("%s%s%s", conf.AppDir.String(), string(os.PathSeparator), savedFilename))
	if err != nil {
		return err
	}
	defer f.Close()
	r := bufio.NewReader(f)
	scan := bufio.NewScanner(r)
	scan.Split(bufio.ScanLines)

	// Define channels and start process to add entries to db
	dbWriteChan := make(chan parsedtxt, 50)
	dbErrChan := make(chan error, 1)
	go handleEntries(conf, dbWriteChan, dbErrChan)
	// Iterate through
	if !scan.Scan() {
		return fmt.Errorf("expected something in buffer")
	}
	i := 0
	for scan.Scan() {
		if i > 1000 {
			break
		}
		if len(scan.Bytes()) < 20 || string(scan.Bytes()[11]) != "(" || string(scan.Bytes()[19]) != ")" {

			continue
		}
		p := parsedtxt{}
		p.oui = string(scan.Bytes()[0:6])

		p.vendor = string(scan.Bytes()[22:len(scan.Bytes())])

		_ = scan.Scan()
		if len(scan.Bytes()) <= 4 {

			select {
			case err = <-dbErrChan:
				return err
			default:
				dbWriteChan <- p
				i++
			}
			continue
		}
		addr1 := string(scan.Bytes()[4:len(scan.Bytes())])

		_ = scan.Scan()
		if len(scan.Bytes()) <= 4 {

			select {
			case err = <-dbErrChan:
				return err
			default:
				dbWriteChan <- p
				i++
			}
			continue
		}
		addr2 := string(scan.Bytes()[4:len(scan.Bytes())])

		p.address = fmt.Sprintf("%s||%s", addr1, addr2)

		_ = scan.Scan()
		if len(scan.Bytes()) <= 4 {

			select {
			case err = <-dbErrChan:
				return err
			default:
				dbWriteChan <- p
				i++
			}
			continue
		}
		p.country = string(scan.Bytes()[4:len(scan.Bytes())])
		select {
		case err = <-dbErrChan:
			return err
		default:
			dbWriteChan <- p
			i++
		}

	}
	// Close to let process know we dont have more data to send.
	close(dbWriteChan)
	// process will send nil to errDone after the chan is closed and last item in buffer is received.
	errDone := <-dbErrChan
	return errDone
}
