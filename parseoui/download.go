package parseoui

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/MrMikeandike/go-oui/ouiconf"
)

// UnzipDownload download and unzips the oui list zipe file.
// The oui list is retrieved from the zipped folder and placed directly into the
// default download folder
func UnzipDownload(config ouiconf.Config) (err error) {
	err = config.AppDir.Verify()
	if err != nil {
		return err
	}
	err = config.DownloadURL.Verify()
	if err != nil {
		return err
	}
	var defaultOuiDownloadPath = config.AppDir.String()
	err = os.Mkdir(defaultOuiDownloadPath, 0755)
	if err != nil && !os.IsExist(err) {
		return err
	}
	// Open tmp file for writing
	localpath := fmt.Sprintf("%s%s%s", defaultOuiDownloadPath, string(os.PathSeparator), savedFilename)
	stat, err := os.Stat(localpath)

	if os.IsExist(err) {
		fmt.Println(stat.ModTime())
		if !stat.ModTime().After(time.Now().Add(-24 * time.Hour)) {
			log.Println("Skipping download because the file exist and has been modified within 24 hrs")
			return nil
		}
		err = os.Remove(localpath)
		if err != nil {
			return err
		}

	}
	log.Printf("downloading file to '%s'\n", localpath)
	f, err := os.Create(localpath)
	if err != nil {
		return err
	}
	defer f.Close()
	// Download oui list
	url := config.DownloadURL.String()
	log.Printf("Downloading file from '%s'\n", url)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Bad http response: %s", resp.Status)
	}
	buff := bytes.NewBuffer([]byte{})

	// Write downloaded oui list to tmp file
	size, err := io.Copy(buff, resp.Body)
	if err != nil {
		return err
	}
	buffReader := bytes.NewReader(buff.Bytes())
	zipReader, err := zip.NewReader(buffReader, size)
	if err != nil {
		log.Println("error @ zipReader definition")
		return err
	}
	for _, unzipped := range zipReader.File {
		fileReader, err := unzipped.Open()
		if err != nil {
			log.Println("error after fileReader")
			return err
		}
		defer fileReader.Close()
		_, err = io.Copy(f, fileReader)
		if err != nil {
			log.Println("error when copying fileReader")
			return err
		}

	}

	return nil

}

// oldDownload will download the oui list zip file
// legacy. PURELY FOR MY OWN PERSONAL TESTING AND BENCHMARKING
func oldDownload(config ouiconf.Config) error {
	err := config.AppDir.Verify()
	if err != nil {
		return err
	}
	err = config.DownloadURL.Verify()
	if err != nil {
		return err
	}
	var defaultOuiDownloadPath = config.AppDir.String()
	err = os.Mkdir(defaultOuiDownloadPath, 0755)
	// Open tmp file for writing
	localpath := fmt.Sprintf("%s%s%s", defaultOuiDownloadPath, string(os.PathSeparator), "oui.txt.zip")
	stat, err := os.Stat(localpath)

	if os.IsExist(err) {
		if !stat.ModTime().After(time.Now().Add(-24 * time.Hour)) {
			log.Println("Skipping download because the file exist and has been modified within 24 hrs")
			return nil
		}
		err = os.Remove(localpath)
		if err != nil {
			return err
		}

	}
	log.Printf("downloading file to '%s'\n", localpath)
	f, err := os.Create(localpath)
	if err != nil {
		return err
	}
	defer f.Close()
	// Download oui list
	url := config.DownloadURL.String()
	log.Printf("Downloading file from '%s'\n", url)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Bad http response: %s", resp.Status)
	}
	// Write downloaded oui list to tmp file
	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return err
	}
	return nil

}
