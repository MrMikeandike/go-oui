package parseoui

import (
	"os"
	"testing"

	"gitlab.com/MrMikeandike/go-oui/ouiconf"
)

func defaultConfig(t *testing.T) (conf ouiconf.Config) {
	t.Helper()
	conf.Defaults()
	conf.DownloadURL = "http://10.0.2.2:9090/oui.txt.zip"
	return conf
}

func TestParseText(t *testing.T) {
	type args struct {
		conf ouiconf.Config
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		struct {
			name    string
			args    args
			wantErr bool
		}{name: "LocalDownload", args: args{defaultConfig(t)}, wantErr: false},
		struct {
			name    string
			args    args
			wantErr bool
		}{name: "verifying_input_params", args: args{ouiconf.Config{}}, wantErr: true},
		struct {
			name    string
			args    args
			wantErr bool
		}{name: "verifying_file_exist", args: args{func() ouiconf.Config {
			c := defaultConfig(t)
			c.AppDir = ouiconf.AppDir(os.TempDir() + string(os.PathSeparator) + ".parseoui")
			return c
		}()}, wantErr: true},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ParseText(tt.args.conf); (err != nil) != tt.wantErr {
				t.Errorf("ParseText() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
