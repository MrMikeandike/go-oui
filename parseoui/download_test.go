package parseoui

import (
	"testing"
)

func TestDownloadZip(t *testing.T) {
	conf := defaultConfig(t)
	err := UnzipDownload(conf)
	if err != nil {
		t.Fatal(err)
	}

}
