package parseoui

import (
	"fmt"
	"os"

	bolt "go.etcd.io/bbolt"

	"github.com/asdine/storm"
	"gitlab.com/MrMikeandike/go-oui/ouiconf"
)

type parsedtxt struct {
	oui     string
	vendor  string
	address string
	country string
}

type OuiEntry struct {
	ID      int    `storm:"id,increment"`
	Oui     string `storm:"unique"`
	Vendor  string `storm:"index"`
	Address string
	Country string
}

func handleEntries(conf ouiconf.Config, parsedChan <-chan parsedtxt, errCh chan<- error) {
	dbPath := conf.AppDir.String() + string(os.PathSeparator) + "oui.db"
	// delete db ?
	db, err := storm.Open(dbPath, storm.BoltOptions(0600, &bolt.Options{}))
	if err != nil {
		errCh <- err
		return
	}
	defer db.Close()
	ouistore := db.From("ouistore")

	err = ouistore.Init(&OuiEntry{})
	if err != nil {
		errCh <- err
		return
	}
	for {
		select {
		case p, more := <-parsedChan:
			if !more {
				// return nil to indicate the job is done
				errCh <- db.Close()
				return
			}
			// do something with new parsed object

			err := ouistore.Save(&OuiEntry{Oui: p.oui, Vendor: p.vendor, Address: p.address, Country: p.country})
			if err != nil {
				errCh <- err
				return
			}

		}
	}

}

func printEntries(conf ouiconf.Config, parsedChan <-chan parsedtxt, errCh chan<- error) {
	i := 0
	for {
		if i > 300 {
			errCh <- fmt.Errorf("testing channel error")
		}

		select {
		case p, more := <-parsedChan:
			if !more {
				fmt.Println("got all jobs")
				errCh <- nil
				return
			}
			fmt.Printf("%d: %+v\n", i, p)
			i++

		}
	}
}
