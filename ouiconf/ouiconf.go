package ouiconf

import (
	"fmt"
	"log"
	"os"
)

/****
Add savedFilename to ouiconf.Config instead of having it be defined in parseoui. Maybe we don't need it...

 ****/

// Config represents all settings that are commonly changed
// the defaults are usually fine
type Config struct {
	// DownloadURL is the url of the oui list to download from
	// currently only supports .zip extension
	DownloadURL DownloadURL

	// AppDir is the directory where the app will keep or cache files
	AppDir AppDir
}

// Defaults returns default settings
func (c *Config) Defaults() {
	c.DownloadURL = "https://linuxnet.ca/ieee/oui.txt.zip"
	homedir, err := os.UserHomeDir()
	if err != nil {
		log.Fatalf("Error getting user home directory.%v", err)
	}

	c.AppDir = AppDir(homedir + string(os.PathSeparator) + ".parseoui")

}

// AppDir is the directory where the app will keep or cache files
type AppDir string

// String returns string type
func (dir AppDir) String() string {
	return string(dir)
}

// Verify does various checks to ensure the value won't cause issues/crashes
func (dir AppDir) Verify() error {
	if dir == "" {
		return &ErrorVerifyingNil{Type: "AppDir"}
	}
	return nil
}

// DownloadURL is the url of the oui list to download from
// currently only supports .zip extension
type DownloadURL string

// String returns string type
func (dloadURL DownloadURL) String() string {
	return string(dloadURL)
}

// Verify does various checks to ensure the value won't cause issues/crashes
func (dloadURL DownloadURL) Verify() error {
	if dloadURL == "" {
		return &ErrorVerifyingNil{Type: "DownloadURL"}
	}
	return nil
}

// ErrorVerifyingNil represents an exit due to bad parameters in Config
type ErrorVerifyingNil struct {
	// Type is the type of variable. aka Config.AppDir has type "AppDir"
	Type string
}

func (e *ErrorVerifyingNil) Error() string {
	return fmt.Sprintf("Error verifying %s. %s can't be null or empty string. Did you forget to set default options with Config's Default() method?", e.Type, e.Type)
}
