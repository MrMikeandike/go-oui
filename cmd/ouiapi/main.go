package main

import (
	"fmt"
	"log"
	"os"

	bolt "go.etcd.io/bbolt"

	"github.com/asdine/storm"

	"gitlab.com/MrMikeandike/go-oui/ouiconf"

	"gitlab.com/MrMikeandike/go-oui/parseoui"
)

func main() {
	conf := ouiconf.Config{}
	conf.Defaults()
	conf.DownloadURL = "http://10.0.2.2:9090/oui.txt.zip"
	err := parseoui.UnzipDownload(conf)
	if err != nil {
		log.Fatalf("Error downloading: %+v\n", err)
	}

	err = parseoui.ParseText(conf)
	if err != nil {
		log.Fatalf("error parsing: %+v\n", err)
	}

	ouis, err := readTest(conf)
	if err != nil {
		log.Fatalf("error reading: %+v\n", err)
	}
	fmt.Printf("length: %d\n", len(ouis))
	for _, v := range ouis {
		fmt.Printf("%+v\n", v)
	}

}

func readTest(conf ouiconf.Config) ([]parseoui.OuiEntry, error) {
	err := conf.AppDir.Verify()
	if err != nil {
		return nil, err
	}

	db, err := storm.Open((conf.AppDir.String() + string(os.PathSeparator) + "oui.db"), storm.BoltOptions(0400, &bolt.Options{}))
	if err != nil {
		return nil, err
	}
	defer db.Close()

	ouistore := db.From("ouistore")
	var ouiList []parseoui.OuiEntry
	err = ouistore.All(&ouiList)
	if err != nil {
		return nil, err
	}
	return ouiList, nil

}
